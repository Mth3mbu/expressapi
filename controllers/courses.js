const Course = require('./../models/Course');
const asyncHandler = require('../middlewares/async');

exports.getCourses = asyncHandler(async (req, res, next) => {
  res.send(res.advancedResults);
});

exports.getCourse = asyncHandler(async (req, res, next) => {
  const courses = await Course.findById(req.params.id).populate({
    path: 'bootcamp',
    select: 'name description _id',
  });

  res.send({ success: true, data: courses });
});

exports.addCourse = asyncHandler(async (req, res, next) => {
  const course = await Course.create(req.body);

  res.send({ success: true, data: course });
});

exports.updateCourse = asyncHandler(async (req, res, next) => {
  const course = await Course.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });
  res.send({ success: true, data: course });
});

exports.deleteCourse = asyncHandler(async (req, res, next) => {
  const course = await Course.findByIdAndDelete(req.params.id);
  res.send({ success: true, data: course });
});
