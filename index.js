const path = require('path');
const express = require('express');
const fileupload = require('express-fileupload');
const dotEnv = require('dotenv');
const connectDB = require('./config/db');
require('colors');

dotEnv.config({ path: './config/config.env' });
const bootcampRoutes = require('./routes/bootcamps');
const cookieParser = require('cookie-parser');
const courseRoutes = require('./routes/courses');
const authRoutes = require('./routes/auth');

const app = express();

app.use(express.json());
app.use(fileupload());
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')));

connectDB();

bootcampRoutes(app);
courseRoutes(app);
authRoutes(app);

const PORT = process.env.PORT || 5000;

const server = app.listen(PORT);

process.on('unhandledRejection', (error, promise) => {
  console.log(`Error: ${error}`.red);
  server.close(() => process.exit(1));
});
