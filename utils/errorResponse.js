class ErrorResponse extends Error {
  constructor(message, statusCode) {
    super(message);
    this.statusCode = statusCode;
    this.errorMessage = message;
  }
}

module.exports = ErrorResponse;
