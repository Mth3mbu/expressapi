const bootcamp = require('../controllers/bootcamps');
const addvanceResults = require('../middlewares/advancedResults');
const Bootcamp = require('../models/Bootcamp');
const { protect, authorize } = require('../middlewares/auth');
const errorHandler = require('../middlewares/error');

module.exports = (app) => {
  app.get('/api/v1/bootcamps/radius/:zipcode/:distance', (req, res) => {
    bootcamp.getBootcampsInRadius(req, res, errorHandler);
  });

  app.get('/api/v1/bootcamps/:id', protect, (req, res) => {
    bootcamp.getBootcampById(req, res, errorHandler);
  });

  app.get(
    '/api/v1/bootcamps',
    addvanceResults(Bootcamp, 'courses'),
    (req, res) => {
      bootcamp.getBootcamps(req, res, errorHandler);
    }
  );

  app.post(
    '/api/v1/bootcamps',
    protect,
    authorize(process.env.USER_ROLES),
    (req, res) => {
      bootcamp.addBootcamp(req, res, errorHandler);
    }
  );

  app.put(
    '/api/v1/bootcamps/:id',
    protect,
    authorize(process.env.USER_ROLES),
    (req, res) => {
      bootcamp.updateBootcamp(req, res, errorHandler);
    }
  );

  app.put(
    '/api/v1/bootcamps/:id/photo',
    protect,
    authorize(process.env.USER_ROLES),
    (req, res) => {
      bootcamp.bootcampPhotoUpload(req, res, errorHandler);
    }
  );

  app.delete(
    '/api/v1/bootcamps/:id',
    protect,
    authorize(process.env.USER_ROLES),
    (req, res) => {
      bootcamp.deleteBootcamp(req, res, errorHandler);
    }
  );
};
