const course = require('../controllers/courses');
const Course = require('../models/Course');
const advanceResults = require('../middlewares/advancedResults');
const errorHandler = require('../middlewares/error');
const { protect, authorize } = require('../middlewares/auth');

module.exports = (app) => {
  app.get(
    '/api/v1/courses',
    advanceResults(Course, {
      path: 'bootcamp',
      select: 'name description _id',
    }),
    (req, res) => {
      course.getCourses(req, res, errorHandler);
    }
  );

  app.get('/api/v1/courses/:id', (req, res) => {
    course.getCourse(req, res, errorHandler);
  });

  app.delete(
    '/api/v1/courses/:id',
    protect,
    authorize(process.env.USER_ROLES),
    (req, res) => {
      course.deleteCourse(req, res, errorHandler);
    }
  );

  app.put(
    '/api/v1/courses/:id',
    protect,
    authorize(process.env.USER_ROLES),
    (req, res) => {
      course.updateCourse(req, res, errorHandler);
    }
  );

  app.post(
    '/api/v1/courses',
    protect,
    authorize(process.env.USER_ROLES),
    (req, res) => {
      course.addCourse(req, res, errorHandler);
    }
  );
};
