const jwt = require('jsonwebtoken');
const asyncHandler = require('./async');
const User = require('../models/User');

exports.protect = asyncHandler(async (req, res, next) => {
  let token;

  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    token = req.headers.authorization.split(' ')[1];
  }
  //   else if (req.cookies.token) {
  //     token = req.cookies.token;
  //   }

  if (!token) {
    return res
      .status(401)
      .send({ error: 'Not authorized to access this route' });
  }

  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRETE_KEY);
    req.user = await User.findById(decoded.userId);

    next();
  } catch (error) {
    res.status(401).send({ error: 'Not authorized to access this route' });
  }
});

exports.authorize = (roles) => {
  return (req, res, next) => {
    if (!roles.split(',').includes(req.user.role)) {
      return res.status(403).send({
        success: false,
        error: `User role ${req.user.role} is not authorized to access this route`,
      });
    }
    next();
  };
};
