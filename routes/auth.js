const authController = require('../controllers/auth');
const errorHandler = require('../middlewares/error');

module.exports = (app) => {
  app.post('/api/v1/auth/register', (req, res) => {
    authController.createUser(req, res, errorHandler);
  });

  app.post('/api/v1/auth/login', (req, res) => {
    authController.signin(req, res, errorHandler);
  });
};
