const advancedResults = (model, populate) => async (req, res, next) => {
  const requestQuery = { ...req.query };
  const unwantedQueryParans = ['select', 'sort', 'page', 'limit'];
  const selectedFields = requestQuery.select
    ? requestQuery.select.split(',').join(' ')
    : [];
  const sortFields = requestQuery.sort
    ? requestQuery.sort.split(',').join(' ')
    : [];

  unwantedQueryParans.forEach((param) => delete requestQuery[param]);

  const queryString = JSON.stringify(requestQuery).replace(
    /\b(gt|gte|lt|lte|in)\b/g,
    (match) => `$${match}`
  );

  let query = model.find(JSON.parse(queryString));

  if (selectedFields.length > 0) {
    query = query.select(selectedFields);
  }

  if (sortFields.length > 0) {
    query = query.sort(sortFields);
  }

  const page = parseInt(req.query.page, 10 || 1);
  const limit = parseInt(req.query.limit, 10 || 100);
  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;
  const total = await model.countDocuments();

  query.skip(startIndex).limit(limit);

  if (populate) {
    query = query.populate(populate);
  }

  const pagination = {};
  const results = await query;
  if (endIndex < total) {
    pagination.next = {
      page: page + 1,
      limit,
    };
  }

  if (startIndex > 0) {
    pagination.prev = {
      page: page - 1,
      limit,
    };
  }

  res.advancedResults = {
    success: true,
    count: results.length,
    pagination,
    data: results,
  };

  next();
};
module.exports = advancedResults;
