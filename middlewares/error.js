const ErrorResponse = require('../utils/errorResponse');
const errorHandler = (error, res) => {
  let message = 'Internal server error';
  let status = 500;
 
  if (error.name === 'MongoError') {
    message = error.message;
  } else if (error.name === 'CastError') {
    message = `Bootcamp with id:${error.value._id} not found`;
    status = 404;
  } else {
    message = error.message;
    status = error?.statusCode || 400;
  }

  res.status(status || 400).send(new ErrorResponse(message, status || 400));
};

module.exports = errorHandler;
