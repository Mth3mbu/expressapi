const path = require('path');
const geocoder = require('../utils/geocoder');
const Bootcamp = require('./../models/Bootcamp');
const asyncHandler = require('../middlewares/async');
const ErrorResponse = require('../utils/errorResponse');

exports.getBootcampById = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findById({ _id: req.params.id });
  res.send({ success: true, data: bootcamp });
});

exports.getBootcamps = asyncHandler(async (req, res, next) => {
  res.send(res.advancedResults);
});

exports.getBootcampsInRadius = asyncHandler(async (req, res, next) => {
  const { zipcode, distance } = req.params;
  const loc = await geocoder.geocode(zipcode);
  const lat = loc[0].latitude;
  const lon = loc[0].longitude;

  const radius = distance / 6378;

  const bootcamps = await Bootcamp.find({
    location: {
      $geoWithin: { $centerSphere: [[lon, lat], radius] },
    },
  });

  res.send({ success: true, data: bootcamps, total: bootcamps.length });
});

exports.updateBootcamp = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.send({ success: true, data: bootcamp });
});

exports.deleteBootcamp = asyncHandler(async (req, res) => {
  const bootcamp = await Bootcamp.findById(req.params.id);

  bootcamp.remove();
  res.send({ success: true, data: bootcamp });
});

exports.bootcampPhotoUpload = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findById(req.params.id);

  if (!req.files) {
    return next(new ErrorResponse('Please upload a file', 400), res);
  }

  const file = await req.files[''];

  if (!file.mimetype.startsWith('image')) {
    return next(new ErrorResponse('Please upload a file', 400), res);
  }

  if (file.size > process.env.MAX_FILE_SIZE) {
    return next(
      new ErrorResponse('Please upload an image less than 1mb', 400),
      res
    );
  }

  file.name = `photo_${bootcamp._id}${path.parse(file.name).ext}`;
  file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async (error) => {
    if (error) {
      return next(
        new ErrorResponse(
          'An error occured while attempting to upload an image',
          500
        ),
        res
      );
    }
    await Bootcamp.findByIdAndUpdate(req.params.id, { photo: file.name });
    res.send({ success: true, photo: file.name });
  });
});

exports.addBootcamp = asyncHandler(async (req, res) => {
  const bootcamp = await Bootcamp.create(req.body);
  res.send({ success: true, data: bootcamp });
});
